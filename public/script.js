for(let i = 1; i <= 18; i++) {
    arrastrarElemento(document.getElementById('plaza'+i));
}

function arrastrarElemento(elementoDePlaza) {
    let posX = 0, posY = 0, dX = 0, dY = 0;
    elementoDePlaza.onpointerdown = arrastrarPuntero;

    function arrastrarPuntero(e) {
        e.preventDefault();
        console.log(e, elementoDePlaza);
        posX = e.clientX;
        posY = e.clientY;
        document.onpointermove = arrastrarElemento;
        document.onpointerup = detenerArrastreElemento;
    }

    function arrastrarElemento(e) {
        dX = e.clientX - posX;
        dY = e.clientY - posY;
        console.log(posX, posY, dX, dY, elementoDePlaza.offsetLeft, elementoDePlaza.offsetTop);
        elementoDePlaza.style.top = elementoDePlaza.offsetTop + dY + 'px';
        elementoDePlaza.style.left = elementoDePlaza.offsetLeft + dX + 'px';
        posX = e.clientX;
        posY = e.clientY;
    }

    function detenerArrastreElemento() {
        document.onpointerup = null;
        document.onpointermove = null;
    }
}